# Mentions légales

## Informations éditeur

Le site <a href="https://challenge.gd4h.ecologie.gouv.fr/" target="_blank">challenge.gd4h.ecologie.gouv.fr<span class="sr-only">ouverture d'une nouvelle fenêtre</span></a> est porté par Datactivist (prestataire) pour le compte du Commissariat Général au Développement Durable (Ministère de la Transition écologique et de la Cohésion des territoires et Ministère de la Transition énergétique).

## Développement

Datactivist<br>
7 bis avenue Saint-Jérôme<br>
13100 Aix-en-Provence

Utilisation de la solution Vitrine.<br>


## Prestataire d’hébergement

Gitlab

## Informations hébergeurs

L'hébergement est assuré via Gitlab Pages, au travers de la solution Vitrine, et prise en charge par la société Datactivist.

Adresse de l’hébergeur :<br>
Gitlab France<br>
57 Rue d’Amsterdam<br>
75008 Paris

## Propriété intellectuelle
#### Marques
Le Commissariat Général au Développement Durable est et restera propriétaire de ses signes distinctifs, à savoir marques, dénominations sociales et autres noms commerciaux, enseignes et noms de domaine. La reproduction, l’imitation ou l’apposition, partielle ou totale des marques, dessins et modèles, qu’ils soient protégés par le droit d’auteur ou le droit de la propriété industrielle, appartenant au Commissariat Général au Développement Durable est strictement interdite sans son accord écrit préalable. Toute utilisation des contenus à des fins commerciales est interdite.

#### Reproduction et réutilisation du contenu
Sauf mention explicite de propriété intellectuelle détenue par des tiers, les contenus sont proposés sous licence ouverte Etalab 2.0. En conséquence, il est possible de les reproduire, copier, modifier, extraire, transformer, communiquer, diffuser, redistribuer, publier, transmettre et exploiter sous réserve de mentionner leur source, leur date de dernière mise à jour et ne pas induire en erreur des tiers quant aux informations qui y figurent.

#### Liens hypertextes
Tout site public ou privé est autorisé à établir, sans autorisation préalable, un lien vers les informations diffusées sur challenge.gd4h.ecologie.gouv.fr.

## Contenu
#### Responsabilité
Le Commissariat Général au Développement Durable ne saurait être tenu pour responsable de tout dommage, quelle qu’en soit la nature, des erreurs typographiques ou des inexactitudes techniques qui pourraient survenir malgré tout le soin apporté par le prestataire Datactivist pour le compte du Commissariat Général au Développement Durable à la rédaction, la modération et la mise en ligne des informations et des documents.

#### Mise à jour
Les informations et les documents contenus sur challenge.gd4h.ecologie.gouv.fr sont susceptibles de faire l’objet d’une mise à jour à tout moment. Le Commissariat Général au Développement Durable ne saurait être tenu pour responsable de tout dommage, quelle qu’en soit la nature, résultant de mises à jour.

#### Liens
Des liens vers d’autres sites, publics ou privés, peuvent vous être proposés afin de faciliter l’accès à l’information de l’utilisateur.
Ces sites tiers n’engagent pas la responsabilité du Commissariat Général au Développement Durable.

#### Disponibilité
Le Commissariat Général au Développement Durable ne garantit pas l’accessibilité à challenge.gd4h.ecologie.gouv.fr notamment en cas de panne et de maintenance ou de force majeure. Le Commissariat Général au Développement Durable ne saurait être tenu pour responsable de tout dommage, quelle qu’en soit la nature, résultant de l’indisponibilité de la ressource.