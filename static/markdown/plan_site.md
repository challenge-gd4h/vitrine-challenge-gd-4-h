# Plan du site

## Site

<a href="." target="_self">Accueil</a>

- <a href="defis" target="_self">Défis</a>
- <a href="calendrier" target="_self">Calendrier</a>
- <a href="faq" target="_self">FAQ</a>
- <a href="reglement" target="_self">Règlement</a>
- <a href="accessibilite" target="_self">Accessibilité</a>
- <a href="mentions_legales" target="_self">Mentions légales</a>
- <a href="donnees_personnelles" target="_self">Données personnelles et cookies</a>

## Forum

<a href="forum.challenge.gd4h.ecologie.gouv.fr" target="_blank" rel="noreferrer">Accueil</a>

- <a href="mentions_legales_forum" target="_self">Mentions légales</a>
- <a href="donnees_personnelles_forum" target="_self">Données personnelles et cookies</a>

<!---
hidden HTML element to force gitlab pages to load those pages
It wouldn't create them if no self links were existing
-->

<a href="consent_data" target="_self" hidden>Consentement au recueil des données</a>
<a href="consent_data_dataposition" target="_self" hidden>Consentement au recueil des données</a>
<a href="defi" target="_self" hidden>Défi</a>
