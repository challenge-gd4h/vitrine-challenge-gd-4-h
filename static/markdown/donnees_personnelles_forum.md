# Données personnelles et cookies

Le site <a href="https://forum.challenge.gd4h.ecologie.gouv.fr/" target="_blank">https://forum.challenge.gd4h.ecologie.gouv.fr<span class="sr-only">ouverture d'une nouvelle fenêtre</span></a> est soumis au Règlement général sur la protection des données 2016/679 (RGPD) et à la Loi Informatique et Libertés n°78-17.

## 1. Traitements des données à caractère personnel

L’outil de mesure d’audience déployé sur ce forum est <a href="https://stats.challenge.gd4h.ecologie.gouv.fr/index.php?idSite=1" target="_blank">Matomo<span class="sr-only">ouverture d'une nouvelle fenêtre</span></a>. Matomo est soumis à la loi Informatique et Libertés.
Les données collectées par Matomo sont anonymisées.

Ces données permettent aux gestionnaires du site d’établir des mesures statistiques de fréquentation et d’utilisation du site. Elles sont conservées sur un serveur hébergé par le fournisseur cloud Scaleway à Paris, France.

De plus, le forum peut être consulté de manière anonyme, mais nécessite un compte dès lors qu’un utilisateur souhaite contribuer aux échanges. Lors de la création d'un compte, plusieurs données à caractères personnelles sont conservées, dont les conditions sont listées ci-après :

#### 1.1 Finalité

Les données à caractère personnel seront collectées pour la bonne création des comptes utilisateurs et le bon fonctionnement du forum. Elles seront accessibles à Datactivist (prestataire), sous-traitant dans le cadre du présent traitement, et aux agents publics impliqués sur le Challenge Green Data for Health au sein de l’Ecolab (MTECT-MTE/CGDD). 

#### 1.2 Base juridique de traitement (article 6A du RGPD)

Ce traitement concerne uniquement les personnes qui souhaitent participer au Challenge Data GD4H.<br>
La base légale du traitement est le recueil du consentement de la personne concernée.

#### 1.2 Données traitées

* L'email de l'utilisateur;
* L'adresse IP de l'utilisateur;
* La navigation de l'utilisateur sur le forum : messages envoyés, sujets vues, dates de connections, etc...

Ces données sont issues du formulaire d'inscription de l'utilisateur ainsi que de l'utilisation qu'il fait du forum lorsqu'il est connecté.

#### 1.3 Lieu et durée de conservation

Ces données sont conservées sur un serveur hébergé par le fournisseur cloud Digital Ocean à Amsterdam, Pays-Bas.<br>
Toutes les données seront supprimées une fois le challenge terminé, au plus tard en Décembre 2023.

#### 1.4 Vos droits

Pour exercer vos droits d'accès, de rectification, de limitation et d’effacement des données personnelles vous concernant, merci d'envoyer un email à cette adresse : <a href="dpd.daj.sg@developpement-durable.gouv.fr" target="_blank">dpd.daj.sg@developpement-durable.gouv.fr<span class="sr-only">ouverture d'une nouvelle fenêtre</span></a>.

<br>

## 2. Cookies

Lors de la consultation du forum <a href="https://forum.challenge.gd4h.ecologie.gouv.fr" target="_blank">https://forum.challenge.gd4h.ecologie.gouv.fr<span class="sr-only">ouverture d'une nouvelle fenêtre</span></a>, des témoins de connexion, dits « cookies », sont déposés sur votre ordinateur, votre mobile ou votre tablette. Lors de votre première visite, un bandeau signale leur présence. 

La CNIL définit les cookies comme « un petit fichier stocké par un serveur dans le terminal (ordinateur, téléphone, etc.) d’un utilisateur et associé à un domaine web (c'est-à-dire dans la majorité des cas à l’ensemble des pages d’un même site web). Ce fichier est automatiquement renvoyé lors de contacts ultérieurs avec le même domaine ».

### 2.1 Cookie Matomo

Le cookie MATOMO_SESSID permet aux gestionnaires du site d’établir des mesures statistiques de fréquentation et d’utilisation du site :
- le cookie déposé sert uniquement à la production de statistiques anonymes
- le cookie ne permet pas de suivre la navigation de l’internaute sur d’autres sites que celui du site forum.challenge.gd4h.ecologie.gouv.fr

### 2.2 Cookie Discourse

De plus, le forum est construit avec <a href="https://www.discourse.org/" target="_blank">Discourse<span class="sr-only">ouverture d'une nouvelle fenêtre</span></a>. Discourse utilise des cookies afin d'assurer le bon fonctionnement du site, ils sont listés ici : 

* **_forum_session** : Cookie session créé lorsque la page est chargée et supprimé lorsque la page est quitée.
* **_t** : Cookie créé lors de la connexion à votre compte, et supprimé lors de la déconnexion ou après 60 jours. Ce cookie permet de ne pas avoir à vous reconnecter entre vos sessions.
* **theme_ids**, **color_scheme_id**, **dark_scheme_id**, **text_size** : Cookie créé lorsque vous modifiez vos préférences pour l'apparence de Discourse. Supprimé après 1 an ou lorsque vos préférences sont réinitialisées aux valeurs par défaut.

Des cookies éphémères peuvent également être utilisés dans d'autre circonstances, notamment au moment de la connection à double facteurs. Pour une liste plus détaillée :
<br><a href="https://meta.discourse.org/t/list-of-cookies-used-by-discourse/83690" target="_blank">Liste des cookies utilisés par Discourse<span class="sr-only">ouverture d'une nouvelle fenêtre</span></a>

* Ces cookies servent uniquement au bon fonctionnement du site ainsi qu'à établir des mesures statistiques d'utilisation du site
* Ces cookies ne permettent pas de suivre la navigation de l’internaute sur d’autres sites que celui du site challenge.gd4h.ecologie.gouv.fr

## 3. Paramétrage des cookies

Vous pouvez paramétrer votre navigateur afin qu’il vous signale la présence de cookies et vous propose de les accepter ou non. Vous pouvez accepter ou refuser les cookies au cas par cas ou bien les refuser une fois pour toutes. A noter, ce paramétrage est susceptible de modifier vos conditions d’accès aux services des sites nécessitant l’utilisation de cookies. Le paramétrage des cookies est différent pour chaque navigateur et en général décrit dans les menus d’aide.

## 4. Les données sont collectées par :

Commissariat Général au Développement Durable

Tour Séquoia 92055

La Défense Cedex

Directeur de publication : Le Commissaire Général au Développement Durable

## 5. En savoir plus sur la réglementation
<a href="https://www.cnil.fr/fr/reglement-europeen-protection-donnees" target="_blank">Le règlement général sur la protection des données (RGPD)<span class="sr-only">ouverture d'une nouvelle fenêtre</span></a>

<a href="https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000886460/" target="_blank">La loi n°78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés<span class="sr-only">ouverture d'une nouvelle fenêtre</span></a>