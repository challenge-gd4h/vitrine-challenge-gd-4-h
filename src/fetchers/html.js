import { fetchText } from "./fetch.js"

export function getSourceRenderType(source) {
	return "content"
}

export async function fetchCardsPageData(source, config, context) {
	throw new Error("Not implemented: html fetcher only returns content, not cards")
}

export async function fetchContentPageData(source, config, context) {
	let html = null
	if (source.content) {
		html = source.content
	} else if (source.file) {
		html = await fetchText(source.file, context)
	}
	return {
		html: html,
		raw: source.raw,
	}
}
