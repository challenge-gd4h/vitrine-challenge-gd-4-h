import { ArrayModel, ObjectModel } from "objectmodel"

import { fetchPageData, loadFetcher } from "../fetchers"
import Zone from "./zone"

const Banner = new ObjectModel({
	classes: [String],
	url: [String],
}).as("Banner")

const CallToActionButton = new ObjectModel({
	title: String,
	url: String,
}).as("CallToActionButton")

const PageModel = new ObjectModel({
	navbarTitle: [String],
	callToActionButton: [CallToActionButton],
	banner: [Banner],
	slug: String,
	subtitle: [String],
	title: [String],
	filterTags: [ArrayModel(ArrayModel(String))],
	zones: [ArrayModel(Zone)],
}).as("Page")

export default class Page extends PageModel {
	async fetchData(config, context, query) {
		async function getSourceData(source) {
			const fetcher = loadFetcher(source.type)
			const extra = {}
			if (query.topic) {
				source.topic = parseInt(query.topic)
				extra.query = source.topic
			}
			const sourceRenderType = fetcher.getSourceRenderType(source)
			if (source.cardsPerLine) {
				extra.cardsPerLine = source.cardsPerLine
			}
			if (source.cardsPerLineSm) {
				extra.cardsPerLineSm = source.cardsPerLineSm
			}
			if (source.disableBlur) {
				extra.disableBlur = source.disableBlur
			}
			if (source.disableReadMore) {
				extra.disableReadMore = source.disableReadMore
			}
			if (source.intermediatePage) {
				extra.intermediatePage = source.intermediatePage
			}
			const data = await fetchPageData(sourceRenderType, fetcher, source, config, context, query)
			return { sourceRenderType, data, extra }
		}
		if (this.source) {
			return await getSourceData(this.source)
		}
		if (this.zones) {
			return await Promise.all(this.zones.map((zone) => getSourceData(zone.source)))
		}
	}
}
