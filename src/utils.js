function lengthUndefined(l) {
	return l.filter((i) => i !== undefined).length
}

export function allAreUndefined(l) {
	return lengthUndefined(l) === 0
}

export function justOneIsDefined(l) {
	return lengthUndefined(l) === 1
}

export function atLeastOneIsDefined(l) {
	return lengthUndefined(l) >= 1
}

export function trimFirstSlash(s) {
	return s ? s.replace(/^\/+/, "") : null
}
